/* Copyright [2021] [Pablo Trujillo]
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
                
                http://www.apache.org/licenses/LICENSE-2.0
                
                Unless required by applicable law or agreed to in writing, software
                distributed under the License is distributed on an "AS IS" BASIS,
                WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                See the License for the specific language governing permissions and
                limitations under the License.
*/

//Pablo Trujillo: Estructura de canción.

package src.estructuras;
import src.estructuras.Playlist;
public class Cancion{
//declaro todas las variables	
        static String nombre;
	static String artista;
	static int duracion;
	static int annoEstreno;
	
	public Cancion(String nombre, String artista, int duracion, int annoEstreno){
		this.nombre=nombre;
		this.artista=artista;
		this.duracion=duracion;
		this.annoEstreno=annoEstreno;
	}
		
//configuro cancion
        
	public Cancion(){
                nombre=nombre;
                artista=artista;
                duracion=duracion;
                annoEstreno=annoEstreno;
	}


	public String getNombre(){
		return nombre;
	}
	public String getArtista(){
		return artista;
	}
	public int getDuracion(){
		return duracion;
	}
	public int getAnnoEstreno(){
		return annoEstreno;
	}

  
//Configuro la forma en la que saldrán los datos a la hora de hacer el mostrar
	@Override
	public String toString(){
		return "\nNombre: "+ getNombre() + "\nArtista: "+ getArtista() + "\nDuración: "+ getDuracion() + "\nAñoEstreno: "+ getAnnoEstreno() + "\n\n";
	}
}
