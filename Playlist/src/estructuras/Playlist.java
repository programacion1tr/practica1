/* Copyright [2021] [Pablo Trujillo]
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
                
                http://www.apache.org/licenses/LICENSE-2.0
                
                Unless required by applicable law or agreed to in writing, software
                distributed under the License is distributed on an "AS IS" BASIS,
                WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                See the License for the specific language governing permissions and
                limitations under the License.
*/

//Pablo Trujillo: Estructura de playlist.

package src.estructuras;

import src.estructuras.Cancion;
import src.interfaz.Interfaz;

import java.io.*;
import java.util.*;

public class Playlist{
	public void Playlist(){

		String nombreFichero= "PlayList.txt";
		File fl = new File(nombreFichero);

//Crea un archivo si no está creado
		
		try{
			fl.createNewFile();
			System.out.println("Archivo " + nombreFichero + " actualizado");
}
		catch(IOException ex){
			System.out.println(nombreFichero + "ya existe");
		}
//Crea una nueva canción y escribe sus datos.
		try{
			Cancion cancion = new Cancion();

			FileWriter flw = new FileWriter(nombreFichero,true);
			flw.write(cancion.toString());
			flw.close();
		}
		catch(IOException exw){
			System.out.println(exw);
		}
	}
}
