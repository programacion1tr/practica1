/* Copyright [2021] [Pablo Trujillo]
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
                
                http://www.apache.org/licenses/LICENSE-2.0
                
                Unless required by applicable law or agreed to in writing, software
                distributed under the License is distributed on an "AS IS" BASIS,
                WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                See the License for the specific language governing permissions and
                limitations under the License.
*/

//Interfaz por Pablo Trujillo

//Creo un package para poder importarlo y que esté más organizado
package src.interfaz;
//importo archivos de java necesarios para el funcionamiento de la interfaz
import java.util.*;
import java.io.*;
import java.lang.*;
//importo estructuras que contiene la cancion y la playlist
import src.estructuras.*;

public class Interfaz{

//Diferentes modos para configurar la forma en la que el usuario ve los diferentes println
        private static String negrita = "\033[1m";
        private static String normal = "\033[0m";
        private static String subraya = "\033[4m";
	
	private static String blanco = "\u001B[37m";
        private static String rojo = "\u001B[31m";
        private static String negro = "\u001B[30m";

//Muestra un resumen de los comandos, las opciones que tienes a la hora de gestionar la playlist y te enseña lo que hace cada función
        private static void mostrarAyuda(){

	System.out.println(negrita + "Nombre" + normal);
        System.out.println("\tPLAYLIST DE MÚSICA");
        System.out.println(negrita + "\nResumen de comandos:" + normal);
        System.out.println(subraya + "\tjava -jar playlistMusica.jar annadir <nombre> <artista> <duracion(en segundos)> <annoestreno> " + normal + "(añade una cancion indicando los datos sobre ella),\n\t" + subraya + "java -jar playlistMusica.jar mostrar" + normal + "(enseña la playlist),\n\t" + subraya + "java -jar playlistMusica.jar ayuda" + normal + "(enseña esta ayuda),\n\t" + subraya + "java -jar playlistMusica.jar editar" + normal + " (permite editar una canción),\n\t" + subraya + "java -jar playlistMusica.jar borrar" + normal + " (permite borrar una canción)\n\t" + subraya + "java -jar playlistMusica.jar actualizar" + normal + " (Actualiza la Hoja De Calculo)");
        System.out.println(negrita + "\nDESCRIPCIÓN" + normal);
        System.out.println("\tAñade y enseña canciones de  tu playlist.");
        }
//Configuro las diferentes opciones para el usuario
        public static void ejecutar(String[] instruccion){
		if (instruccion.length == 0) mostrarAyuda();
//Convierto los int en string para poder meterlos en el txt y creo el txt.
		else if (instruccion[0].equalsIgnoreCase("annadir") && instruccion.length == 5){
			int instruccion3int = Integer.parseInt(instruccion[3]);
			int instruccion4int = Integer.parseInt(instruccion[4]);

			Cancion cancion = new Cancion(instruccion[1], instruccion[2], instruccion3int, instruccion4int);

			Playlist playlist = new Playlist();
			playlist.Playlist();
		}
//lee el archivo ya creado y en caso de no poder emite un mensaje con el tipo de error
		else if (instruccion[0].equalsIgnoreCase("mostrar") && instruccion.length == 1){
		     	try{
				File flr = new File ("PlayList.txt");
				Scanner reader = new Scanner(flr);
				while (reader.hasNextLine()){
					String datos = reader.nextLine();
					System.out.println(datos);
				}
				reader.close();
			}
			catch (FileNotFoundException error) {
				System.out.println("Ha ocurrido un error en la lectura del fichero:" + error);
			}
		}
//muestra la ayuda
		else if (instruccion[0].equalsIgnoreCase("ayuda") && instruccion.length == 1) mostrarAyuda();
//editar canciones
		else if (instruccion[0].equalsIgnoreCase("editar") && instruccion.length == 1){
			try{
			String ArchivoPlaylist = "PlayList.txt";

                        System.out.println("Escriba la canción que desea editar: ");
                        Scanner sc = new Scanner(System.in);
                        String buscado = sc.nextLine();
                        
			System.out.println("Escriba el nuevo nombre de la canción: ");
                        Scanner sc1 = new Scanner(System.in);
                        String nombre = sc.nextLine();
			
			System.out.println("Escriba el nuevo artista de la canción: ");
                        Scanner sc3 = new Scanner(System.in);
                        String artista = sc.nextLine();
			
			System.out.println("Escriba la nueva duración de la canción: ");
                        Scanner sc4 = new Scanner(System.in);
                        String duracion = sc.nextLine();
			
			System.out.println("Escriba el nuevo año de estreno de la canción: ");
                        Scanner sc5 = new Scanner(System.in);
                        String annoEstreno = sc.nextLine();

			Scanner reader = new Scanner(new File(ArchivoPlaylist));

                        StringBuilder guardartxt = new StringBuilder();
                        while (reader.hasNextLine()){
                                String linea = reader.nextLine();
                                guardartxt.append(linea+"\n");
                        }
                        String guardartxt2 = guardartxt.toString();
                        Scanner sc2 = new Scanner(guardartxt2);

                        while (sc2.hasNextLine()){

                                String fraseABuscar = sc2.nextLine();
                                if(fraseABuscar.contains(buscado)) {
                                        String nvotxt = guardartxt2.replace(fraseABuscar, "Nombre: " + nombre);
                                        FileWriter fl = new FileWriter(ArchivoPlaylist);

                                        if (sc2.hasNextLine()){
                                                fraseABuscar = sc2.nextLine();
                                                String nvotxt1 = nvotxt.replace(fraseABuscar, "Artista: " + artista);
                                                
						if (sc2.hasNextLine()){
                                                        fraseABuscar = sc2.nextLine();
                                                        String nvotxt2 = nvotxt1.replace(fraseABuscar, "Duración: " + duracion);

                                                        if (sc2.hasNextLine()){
                                                                fraseABuscar = sc2.nextLine();
                                                                String nvotxt3 = nvotxt2.replace(fraseABuscar, "AñoEstreno: " + annoEstreno);
                                                                System.out.println("La canción <"+ buscado + "> ha sido editada exitosamente");

                                                                fl.write(nvotxt3);
                                                                fl.close();
                                                        }
                                                }
                                        }
                                }
                        }

			}
			catch(IOException er){
				System.out.println(er);
			}
		}	
//borrar canciones
		else if (instruccion[0].equalsIgnoreCase("borrar") && instruccion.length == 1){
			File FficheroNuevo = new File("NuevaPlaylist");
			try{
			String ArchivoPlaylist = "PlayList.txt";
			
			System.out.println("Escriba la canción que desea borrar: ");
			Scanner sc = new Scanner(System.in);
			String buscado = sc.nextLine();
			Scanner reader = new Scanner(new File(ArchivoPlaylist));
			
			StringBuilder guardartxt = new StringBuilder();
			while (reader.hasNextLine()){
				String linea = reader.nextLine();
				guardartxt.append(linea+"\n");
			}
			String guardartxt2 = guardartxt.toString();
			Scanner sc2 = new Scanner(guardartxt2);

			while (sc2.hasNextLine()){
				
				String fraseABuscar = sc2.nextLine();
				if(fraseABuscar.contains(buscado)) {
					String nvotxt = guardartxt2.replace(fraseABuscar, "");
					FileWriter fl = new FileWriter(ArchivoPlaylist);

					if (sc2.hasNextLine()){
						fraseABuscar = sc2.nextLine();
						String nvotxt1 = nvotxt.replace(fraseABuscar, "");
						
						if (sc2.hasNextLine()){
							fraseABuscar = sc2.nextLine();
							String nvotxt2 = nvotxt1.replace(fraseABuscar, "");
							
							if (sc2.hasNextLine()){
								fraseABuscar = sc2.nextLine();
								String nvotxt3 = nvotxt2.replace(fraseABuscar, "");
								
								if (sc2.hasNextLine()){
								fraseABuscar = sc2.nextLine();
								String nvotxt4 = nvotxt3.replace(fraseABuscar, "");
								System.out.println("La canción <"+ buscado + "> ha sido borrada exitosamente");

								fl.write(nvotxt3);
								fl.close();
								}
							}
						}
					}
				}
			}
			}			
			catch(IOException ex){
			System.out.println(ex);
			}
		
		}
//Actualiza la hoja de cálculo
		else if(instruccion[0].equalsIgnoreCase("actualizar") && instruccion.length == 1){
			String ArchivoHoja = "HojaCalculo";
			File flah = new File (ArchivoHoja);
			try{
				flah.createNewFile();
				Scanner reader2 = new Scanner(new File("PlayList.txt"));
				Scanner reader = new Scanner(new File(ArchivoHoja));

				while (reader.hasNextLine()){
					String linea = reader.nextLine();
					linea.replace(linea, "");
					FileWriter guardado = new FileWriter(ArchivoHoja);
					guardado.close();
				}
				while (reader2.hasNextLine()){
					String[] info = new String [4];
					String linea = reader2.nextLine();

					if (linea.contains("Nombre: ")){
						info[0] = linea.replace("Nombre: ", "");
						linea = reader2.nextLine();

						if (linea.contains("Artista: ")){
							info[1] = linea.replace("Artista: ","");
							linea = reader2.nextLine();

							if (linea.contains("Duración: ")){
								info[2] = linea.replace("Duración: ","");
								linea = reader2.nextLine();

								if (linea.contains("AñoEstreno: ")){
									info[3] = linea.replace("AñoEstreno: ","");
									linea = reader2.nextLine();
								}
							
							}
						} 
					}

					if (info [0] !=null || info[1] !=null || info[2] !=null || info[3] !=null){
						FileWriter flah1 = new FileWriter(ArchivoHoja,true);
						String nuevalinea = info[0]+"\t" + info[1] +"\t" + info[2] +"\t" + info[3];
						System.out.println(nuevalinea);
						flah1.write(nuevalinea + "\n");
						flah1.close();
					}
				}
				System.out.println("Archivo " + ArchivoHoja + " ha sido actualizado correctamente");
			}
			catch (IOException ev){
			System.out.println(ev);
			}
		}
	
//si el programa no es capaz de ejecutar ninguna de las instrucciones anteriores te muestra el mensaje
		else{
			System.out.println(rojo + "Error en la instrucción" + normal);
			mostrarAyuda();
		}
	}
}
