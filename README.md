# Playlist #

Este repositorio sirve para manejar tu propia playlist. Puedes añadir, editar, borrar y mostrar las canciones. Para que este repositorio
funcione es necesario tener instalado java y ejecutarlo en un dispositivo con linux.

## ¿Cómo hago que funcione el programa? ##
Para poder ejecutar este programa es necesario hacer los siguientes comandos en el siguiente orden:

1.git clone https://PabloTru@bitbucket.org/programacion1tr/practica1.git

Este comando sirve para copiar el repositorio en su dispositivo.

2.make playlist

Este comando sirve para inicializar el programa y que te muestre la ayuda

## ¿Cómo utilizo el programa? ##
Al iniciar el programa, si has seguido los pasos correctamente te debería de salir lo siguiente:

Nombre PLAYLIST DE MÚSICA

Resumen de comandos:

java -jar playlistMusica.jar annadir <nombre> <artista> <duracion(en segundos)> <annoestreno> (añade una cancion 
indicando los datos sobre ella)

java -jar playlistMusica.jar mostrar(enseña la playlist)

java -jar playlistMusica.jar ayuda(enseña esta ayuda)

java -jar playlistMusica.jar editar (permite editar una canción)

java -jar playlistMusica.jar borrar (permite borrar una canción) 

java -jar playlistMusica.jar actualizar (Actualiza la Hoja De Calculo)


Para utilizarlo debes de seguir los distintos comandos que te aparecen bajo el resumen de comandos. Cada comando 
tiene entre parentesis para que es utilizado, y los comandos te apareceran subrayados.
